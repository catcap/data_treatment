# 1. Description
Module de traitement de données issues de fichiers .csv afin de mettre en forme et nettoyer les recettes de web scraping.

# 2. Prérequis
L'entête du fichier .csv source doit être comporter les colonnes suivantes : project, project_part_title, details, actions, planned_time, total_time, actors_list, created_at, ended_at, title

Toutes les colonnes correspondantes contiennent des chaînes de caractères, les colonnes details et actors_list suivent un formalisme décrit ci-dessous (exprimé sous forme de json passé en chaîne de caractères).

colonne details : `[{""details"":""Ressources :\nresource1\nresource2""},{""details"":""Objectifs:\nobjectif1\nobjecti2""}]`

colonne actors_list : `[{""actors_list"":""actor1""},{""actors_list"":""actor2""},{""actors_list"":""actor3""},{""actors_list"":""actor4""}]`

Différentes sitemaps seront fournie pour couvrir les outils de gestion de projet en ligne les plus courrants (pour le moment, seul beesbusy est couvert).

# 3. Utilisation
Une fois récupéré, le .csv contenant les données doit être placé dans le dossier data_source.
Le script peut-être lancé avec la commande : `python project_extract.py`
Suivez les instructions du terminal.
Les fichiers .json sont stockés dans le dossier ./json.