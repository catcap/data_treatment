import spacy

nlp = spacy.load('fr_core_news_lg')

def define_objective(sentence):
	doc = nlp(sentence)
	
	keywords = {
		'objectif': ['dans l\'objectif de', 'avec pour objectif de', 'en ayant pour objectif de', 'en ayant pour objectif de', 'dans l’objectif de'],
		'atteindre': ['pour atteindre', 'en vue d\'atteindre'],
		'finalité': ['en ayant pour finalité de', 'avec pour finalité de'],
		'but': ['dans le but de', 'avec pour but de'],
		'pour': [ 'pour que', 'pour'],
		'afin': ['afin de', 'afin d\'', 'afin que'],
		'vue': ['en vue de', 'en vue d\’atteindre', 'en vue de réaliser', 'en vue d\'accomplir'],
		'sorte': ['de sorte que'],
		'visant': ['en visant à'],
		'intention': ['dans l\'intention de'],
		'dessein': ['dans le dessein de'],
		'cherchant': ['en cherchant à']
	}

	root = None
	variant = None
	last_word = None
	activity = None
	objective = None
	for i, token in enumerate(doc):
		for key in keywords:
			if token.text == key:
				root = token
				for variant in keywords[root.text]:
					if variant in doc.text:
						variant = variant
						last_word = variant.split()[-1]
						break
			if root is not None:
				break
		if token.text == last_word:
			for j, word in enumerate(doc):
				if j <= i:
					continue
				if word.pos_ == "VERB":
					activity = doc[:j-1]
					objective = doc[j:]
					break
			

	if objective is not None:
		result = {
			'activity': activity.text,
			'objective': objective.text
		}
		return result
	else:
		result = {
			'activity': sentence,
			'objective': None
		}
		return {'activity': doc.text, 'objective': None}