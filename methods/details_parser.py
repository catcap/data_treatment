import re
import spacy
nlp = spacy.load('fr_core_news_lg')

def details_cleaner(details):
	cleaned_details = re.sub(r'[^\w\s/\'-,]', ' ', details)
	cleaned_details = re.sub(r'\s+', ' ', cleaned_details)
	cleaned_details = cleaned_details.strip()
	unwanted_words = ['Ressources', 'Ressource', 'Objectifs', 'Objectif', 'details']
	for word in unwanted_words:
		cleaned_details = cleaned_details.replace(word, '|')
	return cleaned_details

def get_proposition(text):
	doc = nlp(text)
	proposition_heads = [token.head for token in doc if token.dep_ == "ROOT"]
	propositions = []
	for head in proposition_heads:
		proposition = [token.text for token in head.subtree]
		propositions.append(" ".join(proposition))
	return propositions

def extract_resources_and_objectives(cleaned_details):
	doc = nlp(cleaned_details)
	resources = []
	objectives = []
	for sentence in doc.sents:
		is_objective = any(token.pos_ == "VERB" and token.text == token.lemma_ for token in sentence)
		if is_objective:
			objectives.append(sentence.text)
		else:
			last_punct = None
			for i, token in enumerate(sentence):
				if token.pos_ == "PUNCT" or i+1 >= len(sentence):
					if last_punct:
						resources.append(sentence[last_punct+1:i].text)
					elif i+1 >= len(sentence):
						resources.append(sentence[last_punct+1:].text)
						print(sentence[last_punct:].text)
					else:
						resources.append(sentence[:i].text)
					last_punct = i
			

	for i, elem in enumerate(resources):
		resources[i] = elem.replace('|', '').strip()

	for i, elem in enumerate(objectives):
		objectives[i] = elem.replace('|', '').strip()
	
	return {"resources": resources, "objectives": objectives}


def details_parser(details):
	cleaned_details = details_cleaner(details)
	result = extract_resources_and_objectives(cleaned_details)
	return result


details = '[{""details"":""Objectif : détecter les éventuels problèmes de mesure et/ou de traitement des données""},{""details"":""Outil: Devis du client validé et signé, Excel, FAMOS""}]'

print(details_parser(details))
