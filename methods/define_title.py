import spacy

nlp = spacy.load('fr_core_news_lg')

def define_title(sentence):
	sentence = sentence.replace('-', '')
	sentence = sentence.split()
	title = None

	for i, part in enumerate(sentence):
		doc = nlp(part)
		flag = False
		for token in doc:
			if token.is_alpha is False:
				continue
			flag = True
		if flag is False:
			title = ' '.join(sentence[:i])
		if title is not None:
			break
	
	return title

