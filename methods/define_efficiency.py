def define_efficiency(planned_time, total_time):
    time_difference = total_time - planned_time
    if time_difference <= 0:
        efficiency = 5
    elif 0 <= time_difference < 1:
        efficiency = 4
    elif 1 <= time_difference < 2:
        efficiency = 3
    elif 2 <= time_difference < 3:
        efficiency = 2
    else:
        efficiency = 1

    return efficiency
