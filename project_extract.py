import csv
import json
import os
import re
import datetime
import calendar
import locale
locale.setlocale(locale.LC_ALL, '')

from methods import *

def convert_to_json(file_path):
    if file_path.endswith(".csv"):
        return convert_csv_to_json(file_path)
    else:
        raise ValueError("Format de fichier non pris en charge.")

def convert_csv_to_json(file_path):
    # Open the CSV file
    with open(file_path, "r", newline="") as csvfile:
        reader = csv.DictReader(csvfile)

        # Create a dictionary to store the project data
        project_data = {}
        timestamp = datetime.datetime.timestamp(datetime.datetime.now())

        # Iterate through the rows of the CSV file
        for row in reader:
            # Extract the data from the CSV row

            project = row["project"]
            project = define_title(project)

            project_part_title = row["project_part_title"]


            title = row["title"]
            result = define_objective(title)
            activity = result['activity']
            objective = [result['objective']]

            details = row["details"]
            resources = re.search(r'"details":"Ressources?\s*:\s*(?P<resources>.*?)(?=")', details, re.DOTALL | re.IGNORECASE)
            objectives = re.search(r'"details":"Objectifs?\s*:\s*(?P<objectives>.*?)(?=")', details, re.DOTALL | re.IGNORECASE)
            try:
                resources = details.group("resources")
                resources = re.sub(r"\\n|\s*", ",", resources)
                resources = resources.split(",")[1:]
                resources = set(resources)
            except:
                resources = None

            try:
                objectives = objectives.group("objectives")
                objectives = re.sub(r"\\n", ",", objectives)
                objectives = objectives.split(",")[1:]
                if len(objectives) > 0:
                    objective.append(objectives)
            except:
                objectives = None

            actors = row["actors_list"]
            actors = re.sub(r"[\"\[\]{}:]|actors_list", "", actors)
            actors = actors.split(",")

            actions = row["actions"]
            actions = re.sub(r'[\[\]{}:]|(")actions\1', "", actions)
            actions = actions.split(",")
            actions = set(actions)
            actions = [action.replace('"', '') for action in actions]

            planned_time = row["planned_time"]
            planned_time = re.search(r"(?P<planned_time>\d+)\s\w+", planned_time)
            planned_time = float(planned_time.group("planned_time").replace(",", "."))

            total_time = row["total_time"]
            total_time = re.search(r"(?P<total_time>(?:\d+(?:,\d+)?(?:\.\d+)?|,\d+(?:\.\d+)?|\.\d+))\s\w+", total_time)
            total_time = float(total_time.group("total_time").replace(",", "."))

            created_at = row["created_at"]
            if created_at:
                created_at = re.search(r"(?P<day>\b\d+\b)\s(?P<month>\b\w+\b)\s(?P<year>\b\d+\b)\s\w\s(?P<time>\b([01]\d|2[0-3])([:]?)([0-5]\d)\b)", created_at)
                day = int(created_at.group("day"))
                for i, m in enumerate(calendar.month_name):
                    if created_at.group("month") == m:
                        month = i
                year = int(created_at.group("year"))
                hour = int(created_at.group("time")[:2])
                minute = int(created_at.group("time")[3:])
                created_at = datetime.datetime.timestamp(datetime.datetime(year, month, day, hour, minute))

            ended_at = row["ended_at"]
            if ended_at:
                ended_at = re.search(r"(?P<day>\b\d+\b)\s(?P<month>\b\w+\b)\s(?P<year>\b\d+\b)\s\w\s(?P<time>\b([01]\d|2[0-3])([:]?)([0-5]\d)\b)", ended_at)
                day = int(ended_at.group("day"))
                for i, m in enumerate(calendar.month_name):
                    if ended_at.group("month") == m:
                        month = i
                year = int(ended_at.group("year"))
                hour = int(ended_at.group("time")[:2])
                minute = int(ended_at.group("time")[3:])
                ended_at = datetime.datetime.timestamp(datetime.datetime(year, month, day, hour, minute))
                effectiveness = True
            else:
                effectiveness = False

            # Calculate efficiency and performance
            efficiency = define_efficiency(planned_time, total_time)
            performance = define_performance(efficiency, effectiveness=1 if effectiveness else 0)


            # Check if the project already exists in the dataset
            if project in project_data:
                existing_project = project_data[project]
                # If the project exists, check if the project part already exists
                if project_part_title in existing_project["project_parts"]:
                    existing_project_part = existing_project["project_parts"][project_part_title]
                    # If the project part exists, append the activity data to the existing project part
                    existing_project_part["activities"].append({
                        "activity_title": activity,
                        "objectives": objective,
                        "actors": actors,
                        "actions": list(actions) if actions is not None else [],
                        "ressources": list(resources) if resources is not None else [],
                        "performance": {
                            "created_at": created_at,
                            "ended_at": ended_at,
                            "planned_time": planned_time,
                            "total_time": total_time,
                            "efficiency": efficiency,
                            "effectiveness": effectiveness,
                            "performance": performance
                        }
                    })
                else:
                    # If the project part doesn't exist, create a new project part entry with the activity data
                    existing_project["project_parts"][project_part_title] = {
                        "activities": [{
                            "activity_title": activity,
                            "objectives": objective,
                            "actors": actors,
                            "actions": list(actions) if actions is not None else [],
                            "ressources": list(resources) if resources is not None else [],
                            "performance": {
                                "created_at": created_at,
                                "ended_at": ended_at,
                                "planned_time": planned_time,
                                "total_time": total_time,
                                "efficiency": efficiency,
                                "effectiveness": effectiveness,
                                "performance": performance
                            }
                        }]
                    }
            else:
                # If the project doesn't exist, create a new project entry with the project part and activity data
                project_data[project] = {
                    "project_name": project,
                    "project_parts": {
                        project_part_title: {
                            "activities": [{
                                "activity_title": activity,
                                "objectives": objective,
                                "actors": actors,
                                "actions": list(actions) if actions is not None else [],
                                "ressources": list(resources) if resources is not None else [],
                                "performance": {
                                    "created_at": created_at,
                                    "ended_at": ended_at,
                                    "planned_time": planned_time,
                                    "total_time": total_time,
                                    "efficiency": efficiency,
                                    "effectiveness": effectiveness,
                                    "performance": performance
                                }
                            }]
                        }
                    }
                }

            # Write the data to the project's JSON file with UTF-8 encoding
            existing_project_file = f"json/{project.replace(' ', '_')}_{timestamp}.json"
            with open(existing_project_file if os.path.isfile(existing_project_file) else f"json/{project.replace(' ', '_')}_{timestamp}.json", "w", encoding="utf-8") as file:
                json.dump(project_data[project], file, indent=4, ensure_ascii=False)
            print(f"{project.replace(' ', '_')}_{timestamp}.json updated!")



file_path = input('type your file name without its extension > ')
convert_to_json(f"./data_source/{file_path}.csv")
